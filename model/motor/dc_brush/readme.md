Differential equation

电压平衡
$$u_a=Ri_a + L\frac{\text{d}i_a}{\text{d}t} + K\omega$$

感应电动势：
$$E_a=K\omega$$

电磁转矩
$$T_e=Ki_a$$

转矩平衡：
$$T_e=J\frac{\text{d} \omega}{\text{d}t} + B\omega+T_L$$