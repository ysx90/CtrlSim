HEADERS += \
    $$PWD/can_lqr/can_lqr.hpp \
    $$PWD/can_follow_desired/can_follow_desired.hpp \
    $$PWD/can_luenberger/can_luenberger.hpp \
    $$PWD/can_basic_feedback/can_basic_feedback.hpp \
    $$PWD/can_adaptive_ctrl/can_adaptive_ctrl.hpp \
    $$PWD/can_sliding_mode/can_sliding_mode.hpp \
    $$PWD/can_back_step/can_back_step.hpp \
    $$PWD/can_robust_ctrl_compare/can_robust_ctrl_compare.hpp \
    $$PWD/can_kalman_recursive/can_kalman_recursive.hpp \

SOURCES += \
    $$PWD/can_lqr/can_lqr.cpp \
    $$PWD/can_follow_desired/can_follow_desired.cpp \
    $$PWD/can_luenberger/can_luenberger.cpp \
    $$PWD/can_basic_feedback/can_basic_feedback.cpp \
    $$PWD/can_adaptive_ctrl/can_adaptive_ctrl.cpp \
    $$PWD/can_sliding_mode/can_sliding_mode.cpp \
    $$PWD/can_back_step/can_back_step.cpp \
    $$PWD/can_robust_ctrl_compare/can_robust_ctrl_compare.cpp \
    $$PWD/can_kalman_recursive/can_kalman_recursive.cpp \

