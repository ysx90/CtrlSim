
控制对象：
$$\dot{x}=ax^2+u$$

$$\dot{a}=0$$

---

$$\dot{\hat{a}} =-ex^2$$

$$\hat{a} =-\int_0^t{ex^2 \text{d}t}$$

$$u=\dot{x}_d+x^2 \int_0^t{ex^2 \text{d}t}+ke$$