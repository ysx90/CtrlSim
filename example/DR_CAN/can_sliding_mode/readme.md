
$$\dot{x}=f(x)+u$$
$$|f(x)|<\rho(x)$$
$$Let \ \ u=\dot{x}_d + ke + \rho \frac{|e|}{e}$$
$$\Rightarrow\ e= x_{d}-x \rightarrow 0,t \rightarrow \infty$$


---

$$|f(x)|=|a|x^2 \le |\bar{a}|x^2<|\bar{a}|(x^2+0.1)$$

$$ u=ke + \dot{x}_d  + |\bar{a}|(x^2+0.1) \frac{|e|}{e}$$