$$\dot{\omega}_1 = \alpha_{\omega} (\frac{E_q - \lambda \text{sgn}(\omega_1)E_d}{\psi}- \omega_1)$$

$$E_d  = u_d - R_s i_d + \omega_1 Li_q$$
$$E_q  = u_q - R_s i_q + \omega_1 Li_d$$