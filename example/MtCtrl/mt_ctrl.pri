HEADERS += \
    $$PWD/mc_dc_brush_ctrl/mc_dc_brush_ctrl.hpp \
    $$PWD/mc_pmsm_basic_ctrl/mc_pmsm_basic_ctrl.hpp \
    $$PWD/mc_pmsm_ladrc_ctrl/mc_pmsm_ladrc_ctrl.hpp \
    $$PWD/mc_pmsm_ladrc2_ctrl/mc_pmsm_ladrc2_ctrl.hpp \
    $$PWD/mc_pmsm_flux_observer/mc_pmsm_flux_observer.hpp \
    $$PWD/mc_pmsm_scvm_sensorless/mc_pmsm_scvm_sensorless.hpp \
    $$PWD/mc_pmsm_ti_smo/mc_pmsm_ti_smo.hpp \

SOURCES += \
    $$PWD/mc_dc_brush_ctrl/mc_dc_brush_ctrl.cpp \
    $$PWD/mc_pmsm_basic_ctrl/mc_pmsm_basic_ctrl.cpp \
    $$PWD/mc_pmsm_ladrc_ctrl/mc_pmsm_ladrc_ctrl.cpp \
    $$PWD/mc_pmsm_ladrc2_ctrl/mc_pmsm_ladrc2_ctrl.cpp \
    $$PWD/mc_pmsm_flux_observer/mc_pmsm_flux_observer.cpp \
    $$PWD/mc_pmsm_scvm_sensorless/mc_pmsm_scvm_sensorless.cpp \
    $$PWD/mc_pmsm_ti_smo/mc_pmsm_ti_smo.cpp \
